﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace StripeTest2.Controllers
{
    public class PlansController : ApiController
    {
        public IEnumerable<Stripe.StripePlan> GetAllPlans()
        {
            Stripe.StripeListOptions options = new Stripe.StripeListOptions();

            var planSvc = new Stripe.StripePlanService();
            IEnumerable<Stripe.StripePlan> plans = planSvc.List(options);

            return plans;
        }

        public Stripe.StripePlan GetPlan(string planId)
        {
            var planSvc = new Stripe.StripePlanService();
            Stripe.StripePlan plan = planSvc.Get(planId);

            return plan;
        }

        public Stripe.StripePlan CreatePlan(string planId)
        {
            var createPlan = new Stripe.StripePlanCreateOptions();
            createPlan.Name = "";

            var planSvc = new Stripe.StripePlanService();
            Stripe.StripePlan plan = planSvc.Create(createPlan);

            return plan;
        }

        public Stripe.StripePlan UpdatePlan(string planId)
        {
            var updatePlan = new Stripe.StripePlanUpdateOptions();
            updatePlan.Name = "";

            var planSvc = new Stripe.StripePlanService();
            Stripe.StripePlan plan = planSvc.Update(planId, updatePlan);

            return plan;
        }

        public void DeletePlan(string planId)
        {
            var planSvc = new Stripe.StripePlanService();
            planSvc.Delete(planId);
        }
    }
}
