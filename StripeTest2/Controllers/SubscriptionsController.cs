﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StripeTest2.Controllers
{
    public class SubscriptionsController : ApiController
    {
        public Stripe.StripeSubscription UpdateSubscription(string customerId, bool cancel)
        {
            var customerService = new Stripe.StripeCustomerService();

            Stripe.StripeSubscription subscription;

            if (cancel)
            {
                subscription = customerService.CancelSubscription(customerId, true);
            }
            else
            {
                var updateSubscription = new Stripe.StripeCustomerUpdateSubscriptionOptions();

                updateSubscription.TokenId = "";
                updateSubscription.PlanId = "";
                updateSubscription.TrialEnd = DateTime.UtcNow.AddDays(1);

                subscription = customerService.UpdateSubscription(customerId, updateSubscription);
            }

            return subscription;
        }
    }
}
