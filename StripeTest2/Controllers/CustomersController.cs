﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StripeTest2.Controllers
{
    public class CustomersController : ApiController
    {
        public IEnumerable<Stripe.StripeCustomer> GetAllCustomers()
        {
            var customerSvc = new Stripe.StripeCustomerService();
            IEnumerable<Stripe.StripeCustomer> customers = customerSvc.List();

            return customers;
        }

        public Stripe.StripeCustomer GetCustomer(string customerId)
        {
            var customerSvc = new Stripe.StripeCustomerService();
            Stripe.StripeCustomer customer = customerSvc.Get(customerId);

            return customer;
        
        }

        public Stripe.StripeCustomer InsertCustomer()
        {
            var newCustomer = new Stripe.StripeCustomerCreateOptions();

            newCustomer.Email = "";
            newCustomer.Description = "";
            newCustomer.TokenId = "";

            newCustomer.PlanId = "";
            newCustomer.TrialEnd = DateTime.UtcNow.AddDays(7);
            newCustomer.Quantity = 1;

            var customerService = new Stripe.StripeCustomerService();
            Stripe.StripeCustomer createdCustomer = customerService.Create(newCustomer);

            return createdCustomer;
        }

        public Stripe.StripeCustomer UpdateCustomer(string customerId)
        {
            var updateCustomer = new Stripe.StripeCustomerUpdateOptions();

            updateCustomer.Email = "";
            updateCustomer.Description = "";
            updateCustomer.TokenId = "";

            var customerService = new Stripe.StripeCustomerService();
            Stripe.StripeCustomer updatedCustomer = customerService.Update(customerId, updateCustomer);

            return updatedCustomer;
        }

        public void DeleteCustomer(string customerId)
        {
            var customerService = new Stripe.StripeCustomerService();
            customerService.Delete(customerId);
        }
    }
}
